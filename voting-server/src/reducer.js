import { Map } from 'immutable'
import {
  SET_ENTRIES,
  NEXT,
  VOTE
} from './constants'
import {
  INITIAL_STATE,
  setEntries,
  next,
  vote
} from './core'

export default function reducer (state = INITIAL_STATE, action) {
  switch(action.type) {
    case SET_ENTRIES:
      return setEntries(state, action.payload)

    case NEXT:
      return next(state)

    case VOTE:
      return state.update(
        'vote',
        voteState => vote(voteState, action.payload)
      )

    default:
      return state
  }
}
