import { expect } from 'chai'
import { fromJS, Map } from 'immutable'

import {
  SET_ENTRIES,
  NEXT,
  VOTE
} from '../src/constants'
import reducer from '../src/reducer'

describe('reducer', () => {
  it('has an initial state', () => {
    const action = {type: SET_ENTRIES, payload: ['Trainspotting']}
    const nextState = reducer(undefined, action)

    expect(nextState).to.equal(fromJS({
      entries: ['Trainspotting']
    }))
  })

  it('handles SET_ENTRIES', () => {
    const initialState = Map()
    const action = {type: SET_ENTRIES, payload: ['Trainspotting']}
    const nextState = reducer(initialState, action)

    expect(nextState).to.equal(fromJS({
      entries: ['Trainspotting']
    }))
  })

  it('handles NEXT', () => {
    const initialState = fromJS({
      entries: ['Trainspotting', '28 Days Later']
    })
    const action = {type: NEXT}
    const nextState = reducer(initialState, action)

    expect(nextState).to.equal(fromJS({
      vote: {
        pair: ['Trainspotting', '28 Days Later']
      },
      entries: []
    }))
  })

  it('handles VOTE', () => {
    const initialState = fromJS({
      vote: {
        pair: ['Trainspotting', '28 Days Later'],
        tally: {
          Trainspotting: 3,
          '28 Days Later': 2
        }
      },
      entries: []
    })
    const action = {type: VOTE, payload: 'Trainspotting'}
    const nextState = reducer(initialState, action)

    expect(nextState).to.equal(fromJS({
      vote: {
        pair: ['Trainspotting', '28 Days Later'],
        tally: {
          Trainspotting: 4,
          '28 Days Later': 2
        }
      },
      entries: []
    }))
  })

  it('can be used with reduce', () => {
    const actions = [
      {type: SET_ENTRIES, payload: ['Trainspotting', '28 Days Later']},
      {type: NEXT},
      {type: VOTE, payload: 'Trainspotting'},
      {type: VOTE, payload: '28 Days Later'},
      {type: VOTE, payload: 'Trainspotting'},
      {type: NEXT}
    ]
    const finalState = actions.reduce(reducer, Map())

    expect(finalState).to.equal(fromJS({
      winner: 'Trainspotting'
    }))
  })
})
