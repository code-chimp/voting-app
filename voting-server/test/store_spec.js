import { expect } from 'chai'
import { Map, fromJS } from 'immutable'

import makeStore from '../src/store'
import { SET_ENTRIES } from '../src/constants'

describe('store', () => {
  describe('is a Redux store configured with the correct reducer', () => {
    let store

    beforeEach(() => {
      store = makeStore()
    })

    it('returns the correct initial state', () => {
      expect(store.getState()).to.equal(Map())
    })

    it('handles expected actions', () => {
      const action = {
        type: SET_ENTRIES,
        payload: ['Trainspotting', '28 Days Later']
      }

      store.dispatch(action)

      expect(store.getState()).to.equal(fromJS({
        entries: ['Trainspotting', '28 Days Later']
      }))
    })
  })
})
