require('./style.css')

import React from 'react'
import { render } from 'react-dom'
import {
  Router,
  browserHistory
} from 'react-router'
import {
  createStore,
  applyMiddleware
} from 'redux'
import { Provider } from 'react-redux'
import io from 'socket.io-client'

import remoteActionMiddleware from './remote_action_middleware'
import routes from './routes'
import reducer from './reducer'
import { setState } from './actions'

const socket = io(`${location.protocol}//${location.hostname}:8090`)
socket.on('state', (state) => {
  store.dispatch(setState(state))
})

const createStoreWithMiddleware = applyMiddleware(
  remoteActionMiddleware(socket)
)(createStore)
const store = createStoreWithMiddleware(reducer)

render(
  <Provider store={store}>
    <Router history={browserHistory} onUpdate={() => window.scrollTo(0, 0)}>
      {routes}
    </Router>
  </Provider>,
  document.getElementById('app')
)
