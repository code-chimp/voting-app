import {
  SET_STATE,
  VOTE,
  NEXT
} from '../constants'

export function setState (state) {
  return {
    type: SET_STATE,
    payload: state
  }
}

export function vote (entry) {
  return {
    type: VOTE,
    payload: entry,
    meta: {remote: true}
  }
}

export function next () {
  return {
    type: NEXT,
    meta: {remote: true}
  }
}
