import React, { PropTypes } from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { connect } from 'react-redux'

import * as actionCreators from '../actions'
import Loading from '../components/loading'
import Winner from '../components/winner'
import Vote from '../components/vote'

export class Voting extends React.Component {
  constructor (props) {
    super(props)

    this.displayName = 'Voting'

    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
  }

  static propTypes = {
    pair: PropTypes.object,
    hasVoted: PropTypes.string,
    winner: PropTypes.string
  }

  render () {
    const { pair, winner } = this.props
    let content

    if (!pair && !winner) {
      content = <Loading />
    } else if (winner) {
      content = <Winner ref='winner' winner={winner} />
    } else {
      content = <Vote {...this.props} />
    }

    return (
      <div className='voting'>
        {content}
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    pair: state.getIn(['vote', 'pair']),
    hasVoted: state.get('hasVoted'),
    winner: state.get('winner')
  }
}

export const VotingContainer = connect(mapStateToProps, actionCreators)(Voting)
