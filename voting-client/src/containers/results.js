import React, { PropTypes } from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { connect } from 'react-redux'

import * as actionCreators from '../actions'
import Loading from '../components/loading'
import Tally from '../components/tally'
import Winner from '../components/winner'

export class Results extends React.Component {
  constructor (props) {
    super(props)

    this.displayName = 'Results'

    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
  }

  static propTypes = {
    pair: PropTypes.object,
    tally: PropTypes.object,
    winner: PropTypes.string,
    next: PropTypes.func
  }

  render () {
    const { pair, winner } = this.props
    let content

    if (!pair && !winner) {
      content = <Loading />
    } else if (winner) {
      content = <Winner ref='winner' winner={winner} />
    } else {
      content = <Tally {...this.props} />
    }

    return (
      <div className='results'>
        {content}
      </div>
    )
  }

  getVotes (entry) {
    if (this.props.tally && this.props.tally.has(entry)) {
      return this.props.tally.get(entry)
    }

    return 0
  }
}

function mapStateToProps (state) {
  return {
    pair: state.getIn(['vote', 'pair']),
    tally: state.getIn(['vote', 'tally']),
    winner: state.get('winner')
  }
}

export const ResultsContainer = connect(mapStateToProps, actionCreators)(Results)
