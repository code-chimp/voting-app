import React from 'react'
import {
  IndexRoute,
  Route
} from 'react-router'

import App from './components/app'
import { VotingContainer } from './containers/voting'
import { ResultsContainer } from './containers/results'
import FourOhFour from './components/four_oh_four'

const routes = (
  <Route path='/' component={App}>
    <IndexRoute component={VotingContainer} />
    <Route path='/results' component={ResultsContainer} />
    <Route path='*' component={FourOhFour} />
  </Route>
)

export default routes
