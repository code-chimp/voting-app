import React, { PropTypes } from 'react'

export default class App extends React.Component {
  constructor (props) {
    super(props)

    this.displayName = 'App'
  }

  static propTypes = {
    children: PropTypes.node
  }

  render () {
    return (
      <div className='app'>
        {this.props.children}
      </div>
    )
  }
}
