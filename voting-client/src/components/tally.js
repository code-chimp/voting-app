import React, { PropTypes } from 'react'

export default class Tally extends React.Component {
  constructor (props) {
    super(props)

    this.displayName = 'Tally'
  }

  static propTypes = {
    pair: PropTypes.object,
    tally: PropTypes.object,
    winner: PropTypes.string,
    next: PropTypes.func
  }

  render () {
    const {pair, next} = this.props

    return (
      <div className='tally'>
        <div className='tally'>
          {pair.map((entry) => {
            return (
              <div key={entry} className='entry'>
                <h1>{entry}</h1>
                <div className='vote-count'>
                  {this.getVotes(entry)}
                </div>
              </div>
            )
          })}
        </div>
        <div className='management'>
          <button
            className='next next-button'
            onClick={next}>
            Next
          </button>
        </div>
      </div>
    )
  }

  getVotes (entry) {
    if (this.props.tally && this.props.tally.has(entry)) {
      return this.props.tally.get(entry)
    }

    return 0
  }
}
