import React, { PropTypes } from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'

export default class Winner extends React.Component {
  constructor (props) {
    super(props)

    this.displayName = 'Winner'

    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
  }

  static propTypes = {
    winner: PropTypes.string.isRequired
  }

  render () {
    const { winner } = this.props

    return (
      <div className='winner'>Winner is {winner}!</div>
    )
  }
}
