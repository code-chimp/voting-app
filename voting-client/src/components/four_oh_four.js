import React from 'react'

export default class FourOhFour extends React.Component {
  constructor (props) {
    super(props)

    this.displayName = 'FourOhFour'
  }

  render () {
    return (
      <div className='four-oh-four'>
        <h1>Four-Oh-Four: Where Ya Headed Bro?</h1>
      </div>
    )
  }
}
