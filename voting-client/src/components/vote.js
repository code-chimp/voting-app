import React, { PropTypes } from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import classNames from 'classnames'

export default class Vote extends React.Component {
  constructor (props) {
    super(props)

    this.displayName = 'Vote'

    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
  }

  static propTypes = {
    pair: PropTypes.object,
    vote: PropTypes.func,
    hasVoted: PropTypes.string
  }

  render () {
    const { pair, vote } = this.props

    return (
      <div className='vote'>
        {pair.map((entry) => {
          return (
            <button
              key={entry}
              onClick={() => vote(entry)}
              disabled={this.isDisabled()}
              className={classNames({voted: this.hasVotedFor(entry)})}>
              <h1>{entry}</h1>
              {this.hasVotedFor(entry) ? <div className='label'>Voted</div> : null}
            </button>
          )
        })}
      </div>
    )
  }

  isDisabled () {
    return !!this.props.hasVoted
  }

  hasVotedFor (entry) {
    return this.props.hasVoted === entry
  }
}
