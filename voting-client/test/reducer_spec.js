import { expect } from 'chai'
import { List, Map, fromJS } from 'immutable'

import reducer from '../src/reducer'
import {
  SET_STATE,
  VOTE,
  NEXT
} from '../src/constants'

describe('reducer', () => {
  describe('SET_STATE', () => {
    let expectedState

    beforeEach(() => {
      expectedState = fromJS({
        vote: {
          pair: ['Trainspotting', '28 Days Later'],
          tally: {Trainspotting: 1}
        }
      })
    })

    it('handles the action', () => {
      const initialState = Map()
      const action = {
        type: SET_STATE,
        payload: Map({
          vote: Map({
            pair: List.of('Trainspotting', '28 Days Later'),
            tally: Map({Trainspotting: 1})
          })
        })
      }
      const nextState = reducer(initialState, action)

      expect(nextState).to.equal(expectedState)
    })

    it('handles a plain js payload', () => {
      const initialState = Map()
      const action = {
        type: SET_STATE,
        payload: {
          vote: {
            pair: ['Trainspotting', '28 Days Later'],
            tally: {Trainspotting: 1}
          }
        }
      }
      const nextState = reducer(initialState, action)

      expect(nextState).to.equal(expectedState)
    })

    it('handles undefined initial state', () => {
      const action = {
        type: SET_STATE,
        payload: {
          vote: {
            pair: ['Trainspotting', '28 Days Later'],
            tally: {Trainspotting: 1}
          }
        }
      }
      const nextState = reducer(undefined, action)

      expect(nextState).to.equal(expectedState)
    })

    it('removes hasVoted if the pair changes', () => {
      const initialState = fromJS({
        vote: {
          pair: ['Trainspotting', '28 Days Later'],
          tally: {Trainspotting: 1}
        },
        hasVoted: 'Trainspotting'
      })
      const action = {
        type: SET_STATE,
        payload: {
          vote: {
            pair: ['Sunshine', 'Slumdog Millionaire']
          }
        }
      }
      const nextState = reducer(initialState, action)

      expect(nextState).to.equal(fromJS({
        vote: {
          pair: ['Sunshine', 'Slumdog Millionaire']
        }
      }))
    })
  })

  describe('VOTE', () => {
    let initialState

    beforeEach(() => {
      initialState = fromJS({
        vote: {
          pair: ['Trainspotting', '28 Days Later'],
          tally: {Trainspotting: 1}
        }
      })
    })

    it('sets hasVoted', () => {
      const action = {
        type: VOTE,
        payload: 'Trainspotting'
      }
      const nextState = reducer(initialState, action)

      expect(nextState).to.equal(fromJS({
        vote: {
          pair: ['Trainspotting', '28 Days Later'],
          tally: {Trainspotting: 1}
        },
        hasVoted: 'Trainspotting'
      }))
    })

    it('does not set hasVoted on invalid entry', () => {
      const action = {
        type: VOTE,
        payload: 'Sunshine'
      }
      const nextState = reducer(initialState, action)

      expect(nextState).to.equal(initialState)
    })
  })

  describe('NEXT', () => {
    //
  })
})
