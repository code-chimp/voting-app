import { expect } from 'chai'
import {
  mount,
  render,
  shallow
} from 'enzyme'
import React from 'react'
import { List } from 'immutable'

import { Voting } from '../../src/containers/voting'

describe('Voting (enzyme)', () => {
  const pair = List.of('Trainspotting', '28 Days Later')

  it('renders a pair of buttons', () => {
    const wrapper = render(<Voting pair={pair} />)
    const buttons = wrapper.find('button')

    expect(buttons).to.have.length(2)
  })
})
